[![pipeline status](https://gitlab.com/matthewwilliamson93/yesterdays-top-hits/badges/main/pipeline.svg)](https://gitlab.com/matthewwilliamson93/yesterdays-top-hits/-/commits/main)

# Yesterdays Top Hits

A repo to manage updates to the series of Yesterday's Top Hits Playlists

## Getting started

All commands needed should be found in the local `Makefile`.


## Running

This script is run automatically from by the [`.gitlab-ci.yml`](https://gitlab.com/matthewwilliamson93/yesterdays-top-hits/-/blob/main/.gitlab-ci.yml) using the built in [schedule](https://gitlab.com/matthewwilliamson93/yesterdays-top-hits/-/pipeline_schedules) feature.
This runs every Sunday at Noon.
