"""
A script to update the Spotify Playlists for Yesterday's Top Hits.
"""

import datetime
from functools import cache
import os
from typing import TypedDict

from bs4 import BeautifulSoup
from dateutil.relativedelta import relativedelta
import requests


SPOTIFY_AUTH = "https://accounts.spotify.com/api/token/"
SPOTIFY_BASE = "https://api.spotify.com/v1/"


class SongInfo(TypedDict):
    """
    An object used to defined the unique attributes of a song.
    """

    song: str
    artist: str


class SpotifyHeader(TypedDict):
    """
    Type information for headers used to call Spotify.
    """

    Authorization: str


def fetch_n_years_ago_html(years_ago: int) -> str:
    """
    Fetches the content to parse of the billboards from N years ago.

    Args:
        years_ago: Number of years ago to go back.

    Returns:
        The HTML of the site.
    """
    fetch_date = datetime.date.today() - relativedelta(years=years_ago)
    url = f"https://www.billboard.com/charts/hot-100/{fetch_date.strftime('%Y-%m-%d')}/"

    response = requests.get(url)

    if not response.ok:
        raise Exception("Failed to fetch data")

    return response.text


def parse_list_out(site_content: str) -> list[SongInfo]:
    """
    Parses out a list of songs and artists from HTML content.

    Args:
        site_content: The HTML content.

    Returns:
        A list of song / artists objects.
    """
    soup = BeautifulSoup(site_content)

    return [
        {
            "song": row.find("h3", "c-title").text.strip(),
            "artist": row.find("h3", "c-title").find_next_sibling().text.strip(),
        }
        for row in soup.find_all("div", "o-chart-results-list-row-container")
    ]


@cache
def create_spotify_auth_headers() -> SpotifyHeader:
    """
    Creates Spotify headers for authorization.

    Returns:
        The authorization headers for calling Spotify.
    """
    response = requests.post(
        SPOTIFY_AUTH,
        {
            "grant_type": "client_credentials",
            "client_id": os.environ["SPOTIFY_CLIENT_ID"],
            "client_secret": os.environ["SPOTIFY_CLIENT_SECRET"],
        },
    )

    return {"Authorization": f"Bearer {response.json()['access_token']}"}


def get_spotify_track_uri(track: str, artist: str) -> str:
    """
    Implements the Spotify API to search for a specific track.

    https://developer.spotify.com/documentation/web-api/reference/#/operations/search

    Args:
        track: The name of the track
        artist: The artist name

    Returns:
        A Spotify track URI
    """
    response = requests.get(
        f"{SPOTIFY_BASE}search",
        headers=create_spotify_auth_headers(),
        params={"q": f"track:{track}+artist:{artist}", "type": "track"},
    )

    return response.json()["tracks"]["items"][0]["uri"]


def get_playlist_items(playlist_id: str):
    """
    Implements the Spotify API to get a list of tracks from a playlist.

    https://developer.spotify.com/documentation/web-api/reference/#/operations/get-playlists-tracks

    Args:
        playlist_id: The ID of the playlist to load.

    Returns:
        The list of items in a playlist.
    """
    response = requests.get(
        f"{SPOTIFY_BASE}playlists/{playlist_id}/tracks",
        headers=create_spotify_auth_headers(),
        params={"limit": 50},
    )

    return response.json()["items"]


def change_playlist_details(playlist_id: str, description: str) -> bool:
    """
    Implements the Spotify API to change the name and description of a playlist.

    https://developer.spotify.com/documentation/web-api/reference/#/operations/change-playlist-details

    Args:
        playlist_id: The ID of the playlist to change.
        description: The description of the playlist.

    Returns:
        True if the API call was successful, otherwise False.
    """
    response = requests.put(
        f"{SPOTIFY_BASE}playlists/{playlist_id}/",
        headers=create_spotify_auth_headers(),
        data={"description": description},
    )

    return response.status_code == 200


def add_items_to_playlist(playlist_id: str, track_ids: list[str]) -> bool:
    """
    Implements the Spotify API to add tracks to a playlist.

    https://developer.spotify.com/documentation/web-api/reference/#/operations/add-tracks-to-playlist

    Args:
        playlist_id: The ID of the playlist to add playlists to.
        track_ids: A list of tracks IDs to add.

    Returns:
        True if the API call was successful, otherwise False.
    """
    response = requests.post(
        f"{SPOTIFY_BASE}playlists/{playlist_id}/tracks",
        headers=create_spotify_auth_headers(),
        data={"uris": track_ids},
    )

    return response.status_code == 201


def remove_items_from_playlist(playlist_id: str, track_ids: list[str]) -> bool:
    """
    Implements the Spotify API to remove tracks from a playlist.

    https://developer.spotify.com/documentation/web-api/reference/#/operations/remove-tracks-playlist

    Args:
        playlist_id: The ID of the playlist to remove playlists from.
        track_ids: A list of tracks IDs to remove.

    Returns:
        True if the API call was successful, otherwise False.
    """
    response = requests.delete(
        f"{SPOTIFY_BASE}playlists/{playlist_id}/tracks",
        headers=create_spotify_auth_headers(),
        data={"tracks": [{"uri": track_id} for track_id in track_ids]},
    )

    return response.status_code == 200


def main() -> None:
    """
    The main driver for the script.
    """
    print("TODO: Implement the main driver")


if __name__ == "__main__":
    main()
