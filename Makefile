PYTHON    ?= python3.10
SRC        = src
SRC_FILES := $(shell find $(SRC) -name "*.py")

.PHONY: help
help:
	@echo "Usage:"
	@echo "    help:             Prints this screen"
	@echo "    install-dev-deps: Installs dev dependencies"
	@echo "    install-deps:     Installs dependencies"
	@echo "    check-fmt:        Checks the code for style issues"
	@echo "    fmt:              Make the formatting changes directly to the project"
	@echo "    lint:             Lints the code"
	@echo "    run:              Run the script"
	@echo "    clean:            Clean out temporaries"
	@echo ""

.PHONY: install-dev-deps
install-dev-deps:
	$(PYTHON) -m pip install --no-cache-dir -r requirements-dev.txt

.PHONY: install-deps
install-deps:
	$(PYTHON) -m pip install --no-cache-dir -r requirements.txt

.PHONY: check-fmt
check-fmt:
	@echo "Format Checking"
	$(PYTHON) -m black --check $(SRC)

.PHONY: fmt
fmt:
	@echo "Auto Formatting"
	$(PYTHON) -m black $(SRC)

.PHONY: lint
lint:
	@echo "Type Checking"
	@$(PYTHON) -m mypy --ignore-missing-imports $(SRC_FILES)
	@echo "Linting"
	@$(PYTHON) -m pylint $(SRC_FILES)

.PHONY: run
run:
	$(PYTHON) $(SRC)/update_playlists.py

.PHONY:
clean:
	@echo "Removing temporary files"
	@rm -rf "*.pyc" "__pycache__" ".mypy_cache"
